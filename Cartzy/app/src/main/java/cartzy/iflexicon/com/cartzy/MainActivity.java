package cartzy.iflexicon.com.cartzy;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;

import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cartzy.iflexicon.com.cartzy.RecyclerView.common.data.AbstractDataProvider;
import cartzy.iflexicon.com.cartzy.RecyclerView.common.data.AbstractExpandableDataProvider;
import cartzy.iflexicon.com.cartzy.RecyclerView.common.fragment.ExampleDataProviderFragment;
import cartzy.iflexicon.com.cartzy.RecyclerView.common.fragment.ExampleExpandableDataProviderFragment;
import cartzy.iflexicon.com.cartzy.RecyclerView.common.fragment.ExpandableItemPinnedMessageDialogFragment;
import cartzy.iflexicon.com.cartzy.RecyclerView.common.fragment.ItemPinnedMessageDialogFragment;
import cartzy.iflexicon.com.cartzy.RecyclerView.demo_ds.RecyclerListViewFragment;
import cartzy.iflexicon.com.cartzy.RecyclerView.demo_eds.RecyclerExpandableListViewFragment;
import cartzy.iflexicon.com.cartzy.fragments.BarCodeFragment;
import cartzy.iflexicon.com.cartzy.fragments.PeopleFragment;
import cartzy.iflexicon.com.cartzy.fragments.ReceiptFragment;
import cartzy.iflexicon.com.cartzy.fragments.ShopFragment;
import cartzy.iflexicon.com.cartzy.fragments.UserProfileFragment;
import cartzy.iflexicon.com.cartzy.fragments.MyListsFragment;
import cartzy.iflexicon.com.cartzy.providers.BusProvider;
import me.arulnadhan.recyclerview.expandable.RecyclerViewExpandableItemManager;

public class MainActivity extends AppCompatActivity
        implements ExpandableItemPinnedMessageDialogFragment.EventListener,
                   ItemPinnedMessageDialogFragment.EventListener{
    private static final int RC_SIGN_IN = 0;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authListener;

    private DrawerLayout drawerLayout;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBar actionBar;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabase;

    private static final String FRAGMENT_TAG_EXPANDABLE_DATA_PROVIDER = "expandable data provider";
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_EXPANDABLE_LIST_VIEW = "expandable list view";
    private static final String FRAGMENT_LIST_VIEW = "list view";
    private static final String FRAGMENT_TAG_ITEM_PINNED_DIALOG = "item pinned dialog u";

    private ProgressDialog loadingDialog;

    // generate sign in buttons
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize facebook sdk
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_main);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        initToolbar();
        setupDrawerLayout();

        // display first page
        displayView(0, getString(R.string.my_lists));
        displayLists();

        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() != null || AccessToken.getCurrentAccessToken()!= null) {
            Log.d("AUTH","user logged out");
        }else {

            startActivity(new Intent(this, LoginActivity.class));
        }

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("AUTH","user signed in ");

                } else {
                    // User is signed out
                }
                // ...
            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(Cartzy.getBusInstance());
    }

    // Check user is already logged in or not
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_SIGN_IN){
            if(requestCode == RESULT_OK){
                //user logged in
                // get user details
                Log.d("AUTH",auth.getCurrentUser().getDisplayName());
                Log.d("AUTH",auth.getCurrentUser().getEmail());
                Log.d("Auth",auth.getCurrentUser().getPhotoUrl().toString());
                //can get users photo uri
            }else {
                //user not authenticated
                Log.d("AUTH","not authenticated");
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    private void setupDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final NavigationView nav_view = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                //updateChartCounter(nav_view, R.id.nav_cart, global.getCartItem())
                long count = 0 ;
                mDatabase = FirebaseDatabase.getInstance().getReference("users").child(currentUser.getUid()).child("requests");
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        long count = dataSnapshot.getChildrenCount();
                        TextView view = (TextView)nav_view.getMenu().findItem(R.id.nav_people).getActionView().findViewById(R.id.counter);
                        view.setText(String.valueOf(count));
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                super.onDrawerOpened(drawerView);
            }
        };
        // Set the drawer toggle as the DrawerListener
        drawerLayout.addDrawerListener(mDrawerToggle);
        //updateChartCounter(nav_view, R.id.nav_cart, global.getCartItem());

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                actionBar.setTitle(menuItem.getTitle());
                displayView(menuItem.getItemId(), menuItem.getTitle().toString());
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        //super.onSaveInstanceState(outState, outPersistentState);
    }

    private void displayLists(){
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Fetching your Lists...");
        loadingDialog.show();

        mDatabase = FirebaseDatabase.getInstance().getReference("users").child(currentUser.getUid()).child("lists");

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                long count = dataSnapshot.getChildrenCount();
                if(Long.valueOf(count) != null && dataSnapshot.getChildrenCount()>0){
                    getSupportFragmentManager().beginTransaction()
                            .add(new ExampleExpandableDataProviderFragment(), FRAGMENT_TAG_EXPANDABLE_DATA_PROVIDER)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_content, new RecyclerExpandableListViewFragment(), FRAGMENT_EXPANDABLE_LIST_VIEW)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                } else{
                    displayView(0, getString(R.string.my_lists));
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void displayProductsList(String groupPosition){
        Bundle budl = new Bundle();
        budl.putString("position", String.valueOf(groupPosition));

        Fragment exampleDataProviderFragment = new ExampleDataProviderFragment();
        exampleDataProviderFragment.setArguments(budl);

        getSupportFragmentManager().beginTransaction()
                .add(exampleDataProviderFragment, FRAGMENT_TAG_DATA_PROVIDER)
                .addToBackStack(null)
                .commitAllowingStateLoss();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_content, new RecyclerListViewFragment(), FRAGMENT_LIST_VIEW)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.action_my_account:
                displayView(R.layout.fragment_user_profile, "My Profile");
                break;
            case R.id.action_map:
                Intent mapIntent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(mapIntent);
                break;
            case R.id.action_lists:
                displayLists();
                break;
            case R.id.action_barcode:
                displayView(R.layout.fragment_bar_code, "Barcode Scanner");
                break;
            case R.id.action_sync:
                displayLists();
                break;
            case R.id.action_logout:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Log.d("AUTH","User Logged Out");
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                intent.putExtra("finish", true);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                                startActivity(intent);
                                finish();
                            }
                        });
                break;
            case R.id.action_about: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("About");
                builder.setMessage(getString(R.string.about_text));
                builder.setNeutralButton("OK", null);
                builder.show();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    private void displayView(int id, String title) {
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        switch (id) {
            case 0:
                fragment = new MyListsFragment();
                break;
            case R.id.nav_my_lists:
                displayLists();
                break;
            case R.id.nav_people:
                fragment = new PeopleFragment();
                break;
            case R.id.nav_shop_list:
                fragment = new ShopFragment();
                break;
            case R.id.nav_receipts:
                fragment = new ReceiptFragment();
                break;
            case R.layout.fragment_user_profile:
                fragment = new UserProfileFragment();
                break;
            case R.layout.fragment_bar_code:
                fragment = new BarCodeFragment();
                break;
            case R.id.nav_settings:
                Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);
                break;
            default:
                break;
        }

        if (fragment != null) {
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            initToolbar();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    @Override
    public void onBackPressed() {
//        final Context ac = this.getApplicationContext();
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage("Are you sure you want to exit from Cartzy?")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        ExitActivity.exitApplication(ac);
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
//        AlertDialog alert = builder.create();
//        alert.show();

        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to exit from Cartzy?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MainActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else{
            super.onBackPressed();
        }
    }


    /**
     * This method will be called when a group item is removed
     *
     * @param groupPosition The position of the group item within data set
     */
    public void onGroupItemRemoved(int groupPosition) {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.container),
                R.string.snack_bar_text_group_item_removed,
                Snackbar.LENGTH_LONG);

        snackbar.setAction(R.string.snack_bar_action_undo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGroupItemUndoActionClicked();
            }
        });
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.snackbar_action_color_done));
        snackbar.show();
    }

    /**
     * This method will be called when a child item is removed
     *
     * @param groupPosition The group position of the child item within data set
     * @param childPosition The position of the child item within the group
     */
    public void onChildItemRemoved(int groupPosition, int childPosition) {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.container),
                R.string.snack_bar_text_child_item_removed,
                Snackbar.LENGTH_LONG);

        snackbar.setAction(R.string.snack_bar_action_undo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGroupItemUndoActionClicked();
            }
        });
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.snackbar_action_color_done));
        snackbar.show();
    }

    /**
     * This method will be called when a group item is pinned
     *
     * @param groupPosition The position of the group item within data set
     * @param place
     */
    public void onGroupItemPinned(int groupPosition, String place) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_EXPANDABLE_LIST_VIEW);

        getExpandableDataProvider().getGroupItem(groupPosition).setPinned(false);
        ((RecyclerExpandableListViewFragment) fragment).notifyGroupItemChanged(groupPosition);

        displayProductsList(place);
    }

    /**
     * This method will be called when a child item is pinned
     *
     * @param groupPosition The group position of the child item within data set
     * @param childPosition The position of the child item within the group
     */
    public void onChildItemPinned(int groupPosition, int childPosition) {
        final DialogFragment dialog = ExpandableItemPinnedMessageDialogFragment.newInstance(groupPosition, childPosition);

        getSupportFragmentManager()
                .beginTransaction()
                .add(dialog, FRAGMENT_TAG_ITEM_PINNED_DIALOG)
                .commit();
    }

    public void onGroupItemClicked(int groupPosition) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_EXPANDABLE_LIST_VIEW);
        AbstractExpandableDataProvider.GroupData data = getExpandableDataProvider().getGroupItem(groupPosition);

        if (data.isPinned()) {
            // unpin if tapped the pinned item
            data.setPinned(false);
            ((RecyclerExpandableListViewFragment) fragment).notifyGroupItemChanged(groupPosition);
        }
    }

    public void onChildItemClicked(int groupPosition, int childPosition) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_EXPANDABLE_LIST_VIEW);
        AbstractExpandableDataProvider.ChildData data = getExpandableDataProvider().getChildItem(groupPosition, childPosition);

        if (data.isPinned()) {
            // unpin if tapped the pinned item
            data.setPinned(false);
            ((RecyclerExpandableListViewFragment) fragment).notifyChildItemChanged(groupPosition, childPosition);
        }
    }

    private void onGroupItemUndoActionClicked() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_EXPANDABLE_LIST_VIEW);
        final long result = getExpandableDataProvider().undoLastRemoval();

        if (result == RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION) {
            return;
        }

        final int groupPosition = RecyclerViewExpandableItemManager.getPackedPositionGroup(result);
        final int childPosition = RecyclerViewExpandableItemManager.getPackedPositionChild(result);

        if (childPosition == RecyclerView.NO_POSITION) {
            // group item
            ((RecyclerExpandableListViewFragment) fragment).notifyGroupItemRestored(groupPosition);
        } else {
            // child item
            ((RecyclerExpandableListViewFragment) fragment).notifyChildItemRestored(groupPosition, childPosition);
        }
    }

    // implements ExpandableItemPinnedMessageDialogFragment.EventListener
    @Override
    public void onNotifyExpandableItemPinnedDialogDismissed(int groupPosition, int childPosition, boolean ok) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_EXPANDABLE_LIST_VIEW);

        if (childPosition == RecyclerView.NO_POSITION) {
            // group item
            getExpandableDataProvider().getGroupItem(groupPosition).setPinned(ok);
            ((RecyclerExpandableListViewFragment) fragment).notifyGroupItemChanged(groupPosition);
        } else {
            // child item
            getExpandableDataProvider().getChildItem(groupPosition, childPosition).setPinned(ok);
            ((RecyclerExpandableListViewFragment) fragment).notifyChildItemChanged(groupPosition, childPosition);
        }
    }

    public AbstractExpandableDataProvider getExpandableDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_EXPANDABLE_DATA_PROVIDER);
        return ((ExampleExpandableDataProviderFragment) fragment).getExpandableDataProvider();
    }



    /**
     * METHODS OF PRODUCT ITEMS
     */


    /**
     * This method will be called when a list item is removed
     *
     * @param position The position of the item within data set
     */
    public void onItemRemoved(int position) {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.container),
                R.string.snack_bar_text_item_removed,
                Snackbar.LENGTH_LONG);

        snackbar.setAction(R.string.snack_bar_action_undo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemUndoActionClicked();
            }
        });
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.snackbar_action_color_done));
        snackbar.show();
    }

    /**
     * This method will be called when a list item is pinned
     *
     * @param position The position of the item within data set
     */
    public void onItemPinned(int position) {
        final DialogFragment dialog = ItemPinnedMessageDialogFragment.newInstance(position);

        getSupportFragmentManager()
                .beginTransaction()
                .add(dialog, FRAGMENT_TAG_ITEM_PINNED_DIALOG)
                .commit();
    }

    /**
     * This method will be called when a list item is clicked
     *
     * @param position The position of the item within data set
     */
    public void onItemClicked(int position) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
        AbstractDataProvider.Data data = getDataProvider().getItem(position);

        if (data.isPinned()) {
            // unpin if tapped the pinned item
            data.setPinned(false);
            ((cartzy.iflexicon.com.cartzy.RecyclerView.demo_ds.RecyclerListViewFragment) fragment).notifyItemChanged(position);
        }
    }

    private void onItemUndoActionClicked() {
        int position = getDataProvider().undoLastRemoval();
        if (position >= 0) {
            final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
            ((cartzy.iflexicon.com.cartzy.RecyclerView.demo_ds.RecyclerListViewFragment) fragment).notifyItemInserted(position);
        }
    }

    // implements ItemPinnedMessageDialogFragment.EventListener
    @Override
    public void onNotifyItemPinnedDialogDismissed(int itemPosition, boolean ok) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);

        getDataProvider().getItem(itemPosition).setPinned(ok);
        ((cartzy.iflexicon.com.cartzy.RecyclerView.demo_ds.RecyclerListViewFragment) fragment).notifyItemChanged(itemPosition);
    }

    public AbstractDataProvider getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((ExampleDataProviderFragment) fragment).getDataProvider();
    }

}