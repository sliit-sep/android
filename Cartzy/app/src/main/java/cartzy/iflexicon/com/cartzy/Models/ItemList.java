package cartzy.iflexicon.com.cartzy.Models;

/**
 * Created by Gayan Kalhara on 12/15/2016.
 */

public class ItemList {

    private String listId;
    private String listName;
    private String location;

    public ItemList() {
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }
}
