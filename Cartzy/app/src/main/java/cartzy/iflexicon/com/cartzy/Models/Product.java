package cartzy.iflexicon.com.cartzy.Models;

/**
 * Created by Gayan Kalhara on 12/15/2016.
 */

public class Product {

    private String name;
    private String pid;

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
