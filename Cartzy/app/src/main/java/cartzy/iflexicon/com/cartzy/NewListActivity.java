package cartzy.iflexicon.com.cartzy;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cartzy.iflexicon.com.cartzy.database.ListOperations;
import cartzy.iflexicon.com.cartzy.fragments.ShopDetailFragment;
import cartzy.iflexicon.com.cartzy.fragments.ShopDialogFragment;

public class NewListActivity extends AppCompatActivity implements ShopDialogFragment.OnCompleteListener{

    String shopID;
    Double shopLong;
    Double shopLat;
    String shopName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_list);

        final EditText title  = (EditText) findViewById(R.id.title);
        final EditText note = (EditText) findViewById(R.id.notes);
        Button savebutton= (Button) findViewById(R.id.bt_save);
        LinearLayout selectShopbtn = (LinearLayout) findViewById(R.id.lyt_category);

        selectShopbtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
//                final Dialog dialog = new Dialog(view.getContext());
//                dialog.setContentView(R.layout.dialog_shop_picker);
//                dialog.setTitle("Title...");
//
//                ImageView dialogButton = (ImageView) dialog.findViewById(R.id.img_close);
//                // if button is clicked, close the custom dialog
//                dialogButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//
//                dialog.show();
                ShopDialogFragment yourDialogFragment = ShopDialogFragment.newInstance(0);
                yourDialogFragment.show(getSupportFragmentManager().beginTransaction(), "DialogFragment");


            }
        }

        );

        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListOperations lo = new ListOperations();
                lo.addList(title.getText().toString(), note.getText().toString(),shopID,shopName,shopLong,shopLat);
            }
        });



    }

    public void onComplete(Bundle args) {
        shopID = args.getString("ShopID");
        shopLong = args.getDouble("ShopLong");
        shopLat = args.getDouble("ShopLat");
        shopName = args.getString("ShopName");
        TextView txtShopName = (TextView) findViewById(R.id.cat_text);
        txtShopName.setText(shopName);
    }
}
