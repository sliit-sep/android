package cartzy.iflexicon.com.cartzy.RecyclerView.common.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import cartzy.iflexicon.com.cartzy.RecyclerView.common.data.AbstractExpandableDataProvider;
import cartzy.iflexicon.com.cartzy.RecyclerView.common.data.ExampleExpandableDataProvider;


public class ExampleExpandableDataProviderFragment extends Fragment {
    private ExampleExpandableDataProvider mDataProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);  // keep the mDataProvider instance
        mDataProvider = new ExampleExpandableDataProvider();

    }

    public AbstractExpandableDataProvider getExpandableDataProvider() {
        return mDataProvider;
    }
}
