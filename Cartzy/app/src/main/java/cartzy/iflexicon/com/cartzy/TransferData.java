package cartzy.iflexicon.com.cartzy;

/**
 * Created by Gayan Kalhara on 12/15/2016.
 */

public class TransferData {
    private String message;
    private String state;

    public TransferData(String message, String state) {
        this.message = message;
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public String getInternetState(){
        return this.state;
    }
}