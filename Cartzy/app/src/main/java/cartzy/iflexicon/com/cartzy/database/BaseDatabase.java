package cartzy.iflexicon.com.cartzy.database;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Gayan Kalhara on 12/12/2016.
 */

public class BaseDatabase {

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    public DatabaseReference mDatabase,point;
}