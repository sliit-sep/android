package cartzy.iflexicon.com.cartzy.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Gayan Kalhara on 12/12/2016.
 */

public class ListOperations extends BaseDatabase {

    public boolean addList(String listName, String notes, String shopId, String shopNname, Double shopLong, Double shopLat) {
        UUID listId = UUID.randomUUID();
        DatabaseReference point2;

        mDatabase = FirebaseDatabase.getInstance().getReference();
        point = mDatabase.child("users").child(currentUser.getUid()).child("lists").child(listId.toString());
        Map<String,Object> newList = new HashMap<String,Object>();
        newList.put("title",listName);
        newList.put("notes",notes);

        Map<String,Object> shopDetails = new HashMap<String,Object>();
        shopDetails.put("shop",shopNname);
        shopDetails.put("shopID",shopId);
        shopDetails.put("shopLong",shopLong);
        shopDetails.put("shopLat",shopLat);
        point2 = point.child("shopDetails");
        point.updateChildren(newList);
        point2.updateChildren(shopDetails);
        return true;

    }
}