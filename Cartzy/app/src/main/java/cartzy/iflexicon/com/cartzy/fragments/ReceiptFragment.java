package cartzy.iflexicon.com.cartzy.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import cartzy.iflexicon.com.cartzy.R;

public class ReceiptFragment extends Fragment {


    private FragmentTabHost mTabHost;

    public ReceiptFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_receipt,container, false);


        mTabHost = (FragmentTabHost)rootView.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("My Receipts").setIndicator("My Receipts"),
                FriendsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Add New").setIndicator("Add New"),
                SearchFriendsFragment.class, null);


        return rootView;
    }

}
