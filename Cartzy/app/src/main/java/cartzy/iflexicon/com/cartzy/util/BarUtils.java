package cartzy.iflexicon.com.cartzy.util;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

import cartzy.iflexicon.com.cartzy.R;

/**
 * Created by Gayan Kalhara on 12/13/2016.
 */

public class BarUtils {
    public static void setStatusBarColor(AppCompatActivity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        }
    }


}
