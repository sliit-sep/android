# Cartzy #
### Smart Shopping App for Android ###

Have you ever returned from grocery shopping and realized you forgot that one item you really needed? Then this app is for you. Using our mobile app you can search for products exist in our supplier database by using text or voice recognition. If the item you’re adding doesn’t exist in our supplier database you can still add it to your list so you can find it from somewhere else when shopping. This app even supports sharing your lists with friends or family members, so that you can have someone else to do the shopping for you.

### Technologies Used ###

* Firebase

### Developers ###

* Gayan Kalhara (Team Leader)
* Hasitha Jayasinghe
* Udesh Hewagama
* Vishva Ratnayake

### Website ###
* [Visit our website for screenshots and demo](https://www.cartzy.me)